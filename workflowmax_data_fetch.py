from xml.etree import ElementTree as ET
import urllib3
import json
from ast import literal_eval
import certifi
import pandas as pd
import datetime
import string

now = datetime.datetime.now()

path="/Volumes/GoogleDrive/My Drive/Projects/workflowmax/output/"
#path="/tmp/workflow_store/output/"

#-- Credintial config--------
api_key="14C10292983D48CE86E1AA1FE0F8DDFE"
account_key="BF7322205B254D79A477C7827E4F8664"

from_date="20130101"
to_date=now.strftime("%Y%m%d")


#--Getting XML tree -------------
def xmlTreeBuilder(url):
    http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
    r = http.request('GET', url.encode('utf-8'))
    dataString = r.data.decode('utf-8')
    dataString=dataString.replace('<Response api-method="List">','')
    dataString=dataString.replace('<Status>OK</Status>','')
    dataString=dataString.replace('</Response>','')
    tree=ET.fromstring(dataString.encode('ascii', 'ignore').decode('ascii'))
    return tree
#===================================================================================
#===================================================================================
#===================================================================================


#--Staff-------------------------
staffURL="https://api.workflowmax.com/staff.api/list?apiKey="+api_key+"&accountKey="+account_key
staffTree=xmlTreeBuilder(staffURL)

staffDF=pd.DataFrame(columns=['Name','ID','Email'],index=None)

for staff in staffTree.findall('Staff'):
    TempStaffDF = pd.DataFrame({'Name': [staff.find('Name').text.translate(None, string.punctuation)],
                           'ID'  : [staff.find('ID').text.translate(None, string.punctuation)],
                           'Email': [staff.find('Email').text.translate(None, string.punctuation)]
                          },
                          index=None
                         )
    staffDF=pd.concat([staffDF,TempStaffDF])

    # print("Name :"+staff.find('Name').text)
    # print("ID   :"+staff.find('ID').text)
    # print("Email:" + staff.find('Email').text)
    # print("---------------")

staffDF.to_csv(path+'staff.csv',header=True,index=False)
del TempStaffDF


#--Search Client-----------------
# query='Visit Ruapehu'
# urlXML="https://api.workflowmax.com/client.api/search?apiKey="+api_key+"&accountKey="+account_key+"&query="+query+""

#--Category----------------------
categoryURL="https://api.workflowmax.com/category.api/list?apiKey="+api_key+"&accountKey="+account_key
categoryTree=xmlTreeBuilder(categoryURL)

categoryDF=pd.DataFrame(columns=['Name','ID'],index=None)

for category in categoryTree.findall('Category'):
    catTempDF = pd.DataFrame({'Name': [category.find('Name').text.encode('utf-8')],
                              'ID': [category.find('ID').text.encode('utf-8')]
                           },
                          index=None
                          )
    categoryDF = pd.concat([categoryDF, catTempDF])
    # print("Category:"+category.find('Name').text)
    # print("ID:"+category.find('ID').text)
    # print("---------------")

categoryDF.to_csv(path+'category.csv',header=True,index=False)
del catTempDF
#####################################################
### Task, Job, client and Timesheet are essential ###
#####################################################

#--Clients------------------------
clientURL="https://api.workflowmax.com/client.api/list?apiKey="+api_key+"&accountKey="+account_key+""
clientTree=xmlTreeBuilder(clientURL)
#.encode('utf-8')
clientDF=pd.DataFrame(columns=['Name','ID','typeName','jobManagerName','jobManagerID'],index=None)

#unicode(client.find('Name').text,'utf-8')
for client in clientTree.findall('Client'):
    #print("Client   :"+client.find('Name').text)
    #print("Client ID:"+client.find('ID').text)
    JobType=''
    JobManager=''
    for JobType in client.findall('Type'):
        print("Type     :" + JobType.find('Name').text)

    for JobManager in client.findall('JobManager'):
        print(JobManager.find('Name').text.translate(None, string.punctuation))

    clTempDF = pd.DataFrame({'Name'    : [client.find('Name').text.translate(None, string.punctuation)],
                           'ID'        : [client.find('ID').text.translate(None, string.punctuation)],
                           'typeName'       :[JobType.find('Name').text.translate(None, string.punctuation)] if JobType!='' else [''],
                           'jobManagerName': [JobManager.find('Name').text.translate(None, string.punctuation) if JobManager!='' else ''],
                           'jobManagerID': [JobManager.find('ID').text.translate(None, string.punctuation) if JobManager!='' else '']
                           },
                          index=None
                          )
    clientDF = pd.concat([clientDF, clTempDF])
    #if JobType in globals(): del JobType
    #if JobManager in globals(): del JobManager


    #del JobManager if JobManager in locals() else print('no job manager to delet')
    #del JobType if JobType in locals() else print('no job type to delet')



        # print("Client   :" + client.find('Name').text)
        # print("Client ID:" + client.find('ID').text)
        # print("Job Manager:"+JobManager.find('Name').text)

    #print("---------------")
clientDF.to_csv(path+'client.csv',header=True,index=False)
del clTempDF
del clientDF

#--Tasks--------------------------
taskURL="https://api.workflowmax.com/task.api/list?apiKey="+api_key+"&accountKey="+account_key
taskTree=xmlTreeBuilder(taskURL)

taskDF=pd.DataFrame(columns=['Name','ID'],index=None)

for task in taskTree.findall('Task'):
    taskTempDF = pd.DataFrame({'Name': [task.find('Name').text],
                               'ID'  : [task.find('ID').text]
                           },
                          index=None
                          )
    taskDF = pd.concat([taskDF, taskTempDF])
    print("Task   :"+task.find('Name').text)
    print("Task ID:"+task.find('ID').text)
    print("---------------")
taskDF.to_csv(path+'task.csv',header=True,index=False)
del taskTempDF

#--Jobs---------------------------
jobURL="https://api.workflowmax.com/job.api/list?apiKey="+api_key+"&accountKey="+account_key+"&from="+from_date+"&to="+to_date
jobTree=xmlTreeBuilder(jobURL)

jobDF=pd.DataFrame(columns=['Name','ID','Type','State','StartDate','DueDate','InternalID','clientName','clientID','staffName','staffID'],index=None)

for job in jobTree.findall('Job'):

    print("Job           :"+job.find('Name').text)
    print("Job ID        :"+job.find('ID').text)
    #print("Description   :"+ job.find('Description').text)
    print("State         :"+ job.find('State').text)
    print("Start Date    :"+ job.find('StartDate').text)
    print("Due Date      :"+ job.find('DueDate').text)
    #print("Completed Date:"+ job.find('CompletedDate').text) # it fails so we need to look it up by its index
    #print("Completed Date:" + job[8].text)
    print("Internal ID   :"+ job.find('InternalID').text)
    print("Type:" + job[6].text if '00:00:00' not in job[6].text else '')
    #print("Job Type   :" + job.find('Type').text)
    print("---------------------")

    for client in job.findall('Client'):
        #if job.find('Name').text not in clientList:clientList.append(job.find('Name').text)
        print("Clien         :"+client.find('Name').text)

    for assign in job.findall('Assigned'):
        staffAssignedCount = len(assign.findall('Staff'))
        print("Staff assigned count:" + str(staffAssignedCount))
        for staff in assign.findall('Staff'):
            jobTempDF = pd.DataFrame({'Name'         : [job.find('Name').text],
                                      'ID'           : [job.find('ID').text],
                                      'Type'         : [job[6].text if '00:00:00' not in job[6].text else ''],
                                      'State'        : [job.find('State').text],
                                      'StartDate'    : [job.find('StartDate').text[:10]],
                                      'DueDate'      : [job.find('DueDate').text[:10]],
                                      'InternalID'   : [job.find('InternalID').text],
                                      'clientName'   : [client.find('Name').text],
                                      'clientID'     : [client.find('ID').text],
                                      'staffName'    : [staff.find('Name').text],
                                      'staffID'      : [staff.find('ID').text]
                                   },
                                  index=None
                                  )
            jobDF = pd.concat([jobDF, jobTempDF])

            #if job.find('Name').text not in clientList:clientList.append(job.find('Name').text)
            print("Staff Name    :"+staff.find('Name').text)
            #print("Staff Id      :" +staff.find('ID').text)

    print("---------------")

jobDF.to_csv(path+'job.csv',header=True,index=False,mode='w')
del jobTempDF
del jobDF

#--Timesheet----------------------------
timesheetURL="https://api.workflowmax.com/time.api/list?apiKey="+api_key+"&accountKey="+account_key+"&from="+from_date+"&to="+to_date
timesheetTree=xmlTreeBuilder(timesheetURL)

timesheetDF=pd.DataFrame(columns=['ID','Date','Minutes','Billable','jobName','jobID','taskName','taskID','staffName','staffID'],index=None)

for timesheet in timesheetTree.findall('Time'):

    print("time ID :" +timesheet.find('ID').text)
    print("Date    :" +timesheet.find('Date').text)
    print("Minutes :" + timesheet.find('Minutes').text)
    print("Billable:" + timesheet.find('Billable').text)

    for job in timesheet.findall('Job'):
        #if job.find('Name').text not in clientList:clientList.append(job.find('Name').text)
        print("Job     :"+job.find('Name').text)
        print("Job ID  :"+job.find('ID').text)

    for task in timesheet.findall('Task'):
        #if job.find('Name').text not in clientList:clientList.append(job.find('Name').text)
        print("Task     :"+task.find('Name').text)
        print("Task ID  :"+task.find('ID').text)

    for staff in timesheet.findall('Staff'):
        #if job.find('Name').text not in clientList:clientList.append(job.find('Name').text)
        print("Staf    :"+staff.find('Name').text)
        print("Staff ID:"+staff.find('ID').text)

        timesheetTempDF = pd.DataFrame({'Date'     : [timesheet.find('Date').text[:10]],
                                        'ID'       : [timesheet.find('ID').text],
                                        'Minutes'  : [timesheet.find('Minutes').text],
                                        'Billable' : [timesheet.find('Billable').text],
                                        'jobName'  : [job.find('Name').text],
                                        'jobID'    : [job.find('ID').text],
                                        'taskName' : [task.find('Name').text],
                                        'taskID'   : [task.find('ID').text],
                                        'staffName': [staff.find('Name').text],
                                        'staffID'  : [staff.find('ID').text]
                               },
                              index=None
                              )

        timesheetDF = pd.concat([timesheetDF, timesheetTempDF])

    print("---------------")

timesheetDF.to_csv(path+'timesheet.csv',header=True,index=False,mode='w')

del timesheetDF
del timesheetTempDF


#===================================================================================
#===================================================================================
#===================================================================================

